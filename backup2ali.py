import os
import oss2
import zipfile
import shutil
import time
import shutil

def make_zip(source_dir, output_filename):
    zipf = zipfile.ZipFile(output_filename, 'w')
    pre_len = len(os.path.dirname(source_dir))
    for parent, dirnames, filenames in os.walk(source_dir):
        for filename in filenames:
            pathfile = os.path.join(parent, filename)
            arcname = pathfile[pre_len:].strip(os.path.sep)
            zipf.write(pathfile, arcname)
    zipf.close()

#Get neccessary environmental variable
oss_endpoint = os.environ['ALICLOUD_OSSENDPOINT']
oss_bucketname = os.environ['ALICLOUD_OSSBUCKETNAME']
ali_accessid = os.environ['ALICLOUD_ACCESSID']
ali_accesskey = os.environ['ALICLOUD_ACCESSKEY']


#Authentication
auth = oss2.Auth(ali_accessid,ali_accesskey)
bucket = oss2.Bucket(auth, oss_endpoint, oss_bucketname)
    

#Timestamp
named_tuple = time.localtime()
time_string = time.strftime("%Y%m%d%H%M%S", named_tuple)


#Zip name
uid = 'clonedbackup'+time_string
tmpdir = '/clonedbackups'
       
#zip file
zipname = '/tmp/'+uid + '.zip'
make_zip(tmpdir , zipname)
print("Archiving all the source code from :"+tmpdir)

#upload
try:
    total_size = os.path.getsize(zipname)
    part_size = oss2.determine_part_size(total_size, preferred_size = 128 * 1024)
    key = uid + '.zip'
    upload_id = bucket.init_multipart_upload(key).upload_id
    with open(zipname, 'rb') as fileobj:
        parts = []
        part_number = 1
        offset = 0
        while offset < total_size:
            num_to_upload = min(part_size, total_size - offset)
            result = bucket.upload_part(key, upload_id, part_number,oss2.SizedFileAdapter(fileobj, num_to_upload))
            parts.append(oss2.models.PartInfo(part_number, result.etag))
            offset += num_to_upload
            part_number += 1
    bucket.complete_multipart_upload(key, upload_id, parts)
    print("Uploaded to OSS")
    print("Total Size:" +str(total_size))
except Exception as e:
    print("Upload failed")
    raise SystemExit(1)

print("Performing check")

try:
    result = bucket.get_object(key)
    print("Upload success!")
except oss2.exceptions.NoSuchKey as e:
    print('{0} not found: http_status={1}, request_id={2}'.format(key, e.status, e.request_id))
    print("Upload failed!")
    raise SystemExit(1)

os.system('rm -rf /clonedbackups/* && rm -rf /tmp/*')
